/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fightreservation.domain;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class FlightsDetail implements Serializable,Comparable<FlightsDetail> {
    public double OutboundFlightPrice;
    public String OutboundAirline;

    public String getOutboundAirline() {
        return OutboundAirline;
    }

    public void setOutboundAirline(String OutboundAirline) {
        this.OutboundAirline = OutboundAirline;
    }
    public double getOutboundFlightPrice() {
        return OutboundFlightPrice;
    }

    public void setOutboundFlightPrice(double OutboundFlightPrice) {
        this.OutboundFlightPrice = OutboundFlightPrice;
    }

    @Override
    public int compareTo(FlightsDetail o) {
        return (int)(OutboundFlightPrice-o.getOutboundFlightPrice());
    }
    

    
}
