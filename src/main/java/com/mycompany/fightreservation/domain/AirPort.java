package com.mycompany.fightreservation.domain;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Room107
 */
public enum AirPort {
    
    BKK("Suvarnabhumi Airport","Bangkok","TH"),
    DMK("Don Mueang International Airport","Bangkok","TH"),
    HKT("Phuket International Airport","Phuket","TH"),
    CNX("Chiang Mai International Airport","ChiangMai","TH"),
    HDY("Hat Yai International Airport","Songkhla","TH"),
    CEI("Nae Fah Luang Chiang Rai International Airport","Chiang Rai","TH");

    AirPort(String airportName,String city,String country){
        this.airportName = airportName;
        this.city = city;
        this.country = country;

    }
    private final String city;
    private final String country;
    private final String airportName;

    public String city(){
        return city;
    }

    public String country(){
        return country;
    }

    public String airportName(){
        return airportName;
    }


}
