/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fightreservation.domain;

import java.util.ArrayList;
import javax.swing.AbstractListModel;

/**
 *
 * @author Administrator
 */
public class CostListModel extends AbstractListModel<CostDetail>{

    private ArrayList<CostDetail> model;
    public CostListModel(){
        model = new ArrayList<CostDetail>();
    }
    
    public ArrayList<CostDetail> getCostArray(){
        return model;
    }
    
    public CostListModel(ArrayList<CostDetail> model){
        this.model = model;
    }
    
    @Override
    public int getSize() {
        return model.size();
    }

    @Override
    public CostDetail getElementAt(int index) {
        return model.get(index);
    }
   
}