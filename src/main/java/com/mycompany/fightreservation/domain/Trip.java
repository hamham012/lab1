package com.mycompany.fightreservation.domain;

import java.util.Calendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Room107
 */
public class Trip {
    private AirPort fromAirport;
    private AirPort toAirport;
    private byte noOfPassengers;
    private Calendar departureDateTime;
    private Calendar returnDateTime;
    private boolean directFlight;
    private TravelClass travelClass;
    private TripType tripType;
    
}
