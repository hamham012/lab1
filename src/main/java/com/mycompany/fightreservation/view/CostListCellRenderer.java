/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fightreservation.view;

import com.mycompany.fightreservation.domain.CostDetail;
import com.mycompany.fightreservation.domain.FlightsDetail;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.border.EtchedBorder;

/**
 *
 * @author Administrator
 */
public class CostListCellRenderer extends JPanel implements ListCellRenderer<CostDetail> {

    private ImageIcon icon;
    
    public CostListCellRenderer(){
        this.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
    }
    
    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
    }
    @Override
    public Component getListCellRendererComponent(JList list, 
            CostDetail value, int index, boolean isSelected, boolean cellHasFocus) {
            
       this.setLayout(new BoxLayout(this,BoxLayout.X_AXIS));
       this.removeAll();
       
       
       String outboundCostName = value.getOutboundCostName();
       JPanel CostNamePanel = new JPanel();
       JLabel CostNameLabel = new JLabel(outboundCostName);
       CostNamePanel.add(CostNameLabel);
       CostNamePanel.setPreferredSize(new Dimension(100, 30));
       CostNamePanel.setMaximumSize(new Dimension(100, 30));
       CostNamePanel.setMinimumSize(new Dimension(100, 30));
       CostNameLabel.setFont(new Font("Courier New",Font.BOLD,14));
       this.add(CostNamePanel);
       
       Component rigidArea1 = Box.createRigidArea(new Dimension(20,20));
       this.add(rigidArea1);
//       
       
       
       double outBoundPrice = value.getOutboundPrice();
       JLabel price1 = new JLabel(String.valueOf(outBoundPrice));
       price1.setFont(new Font("Courier New",Font.BOLD,14));
//       price1.setForeground(Color.BLUE);
       price1.setPreferredSize(new Dimension(70,20));
       price1.setMaximumSize(new Dimension(70,20));
       price1.setMaximumSize(new Dimension(70,20));
       this.add(price1);
//       
      
//       
//       ImageIcon temp = new ImageIcon(outBoundAirline+".jpg");
//       icon = new ImageIcon(temp.getImage().getScaledInstance(50, 50, Image.SCALE_SMOOTH));
//       JPanel picPanel = new JPanel();
//       JLabel picLabel = new JLabel(icon);
//       picPanel.add(picLabel);
////       picPanel.setBackground(Color.red);
//       picPanel.setPreferredSize(new Dimension(50, 50));
//       picPanel.setMaximumSize(new Dimension(50, 50));
//       picPanel.setMinimumSize(new Dimension(50, 50));
//       this.add(picPanel);
//       
//       Component rigidArea2 = Box.createRigidArea(new Dimension(20, 20));
//       this.add(rigidArea2);
//       

//       
//       Component rigidArea3 = Box.createRigidArea(new Dimension(20, 20));
//       this.add(rigidArea3);
//       
//       JPanel detailPanel = new JPanel();
////       JButton detailButton = new JButton("Detail");
//       JLabel detailLabel = new JLabel("Detail");
//       detailPanel.add(detailLabel);
//       detailPanel.setBackground(Color.BLUE);
//       detailPanel.setPreferredSize(new Dimension(70, 40));
//       detailPanel.setMinimumSize(new Dimension(70, 40));
//       detailPanel.setMaximumSize(new Dimension(70, 40));
//       
//       this.add(detailPanel);
//       
       return this;
    }
    
    
}
